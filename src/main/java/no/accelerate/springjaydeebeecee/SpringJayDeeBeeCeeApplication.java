package no.accelerate.springjaydeebeecee;

import no.accelerate.springjaydeebeecee.dao.PgpgpgDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJayDeeBeeCeeApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringJayDeeBeeCeeApplication.class, args);
    }
}
