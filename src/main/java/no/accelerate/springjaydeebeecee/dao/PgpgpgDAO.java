package no.accelerate.springjaydeebeecee.dao;

import no.accelerate.springjaydeebeecee.dao.models.Student;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class PgpgpgDAO {
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String user;
    @Value("${spring.datasource.password}")
    private String password;

    public void testConnection(){
        try (Connection conn = DriverManager.getConnection(url, user, password)){
            System.out.println("Connected");
        } catch (SQLException e) {
            System.out.println(e.getMessage());;
        }
    }
    public List<Student> getAllStudents() {
        List<Student> studs = new ArrayList<>();
        String sql = "SELECT * FROM student";
        try (Connection conn = DriverManager.getConnection(url, user, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet set = statement.executeQuery();
            while(set.next()) {
                studs.add(new Student(
                        set.getInt("stud_id"),
                        set.getString("stud_name"),
                        set.getInt("supervisor_id")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());;
        }
        return studs;
    }

    public Student getStudentById(int id) {
        Student stud = null;
        String sql = "SELECT * FROM student WHERE stud_id = ?";
        try (Connection conn = DriverManager.getConnection(url, user, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            while(set.next()) {
                stud = new Student(
                        set.getInt("stud_id"),
                        set.getString("stud_name"),
                        set.getInt("supervisor_id")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());;
        }
        return stud;
    }

    public int insertStudent(Student student) {
        String sql = "INSERT INTO student VALUES (?,?,?)";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, user,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, student.id());
            statement.setString(2,student.name());
            statement.setInt(3, student.professor());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int updateStudent(Student student) {
        return 1;
    }

    public int deleteStudent(Student student) {
        return 1;
    }
}
