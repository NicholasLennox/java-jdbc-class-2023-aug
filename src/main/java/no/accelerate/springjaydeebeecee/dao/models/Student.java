package no.accelerate.springjaydeebeecee.dao.models;

public record Student(int id, String name, int professor) { }
