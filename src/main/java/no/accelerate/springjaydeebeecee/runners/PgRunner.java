package no.accelerate.springjaydeebeecee.runners;

import no.accelerate.springjaydeebeecee.dao.PgpgpgDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class PgRunner implements CommandLineRunner {
    final PgpgpgDAO dao;

    public PgRunner(PgpgpgDAO dao) {
        this.dao = dao;
    }

    @Override
    public void run(String... args) throws Exception {
        dao.testConnection();
    }
}
