package no.accelerate.springjaydeebeecee.repositories;

import no.accelerate.springjaydeebeecee.dao.models.Student;

public interface StudentRepository extends CRUDRepository<Student> {
    Student findByName(String name);

}
