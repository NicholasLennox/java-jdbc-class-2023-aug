package no.accelerate.springjaydeebeecee.repositories;

import no.accelerate.springjaydeebeecee.dao.models.Student;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentRepositoryImpl implements StudentRepository {
    private String url;
    private String user;
    private String password;

    public StudentRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String user,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    @Override
    public List<Student> getAll() {
        return null;
    }

    @Override
    public Student getById(int id) {
        return null;
    }

    @Override
    public int create(Student obj) {
        return 0;
    }

    @Override
    public int update(Student obj) {
        return 0;
    }

    @Override
    public int delete(int id) {
        return 0;
    }

    @Override
    public Student findByName(String name) {
        return null;
    }
}
